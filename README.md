# ned-hello-world

An example Node Express app with Docker.

### Usage on development workstation

```bash
# Build app image, run container
docker build -t ned-hello-world.alpine:1.0.0 -f ./dockerfiles/alpine.Dockerfile .
# Login with your Docker Hub credentials
docker login
docker tag ned-hello-world.alpine:1.0.0 iansmcb/ned-hello-world.alpine:1.0.0
docker run -ti --rm -p 3000:3000 --name ned ned-hello-world.alpine

# Build tools image, run container
docker build -t ned-hello-world-tools.alpine -f ./dockerfiles/tools-alpine.Dockerfile .
docker run -ti --rm -p 3000:3000 -v "$PWD"/src/:/app/src --name ned-tools ned-hello-world-tools.alpine
```

### Usage on a Kubernetes cluster

```bash
helm install -n ned --create-namespace ned chart/ned-hello-world
```
