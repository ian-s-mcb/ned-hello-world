FROM node:15-alpine

# Change working directory
WORKDIR "/app"

# Update packages and install dependency packages for services
RUN apk upgrade

# Copy package.json and package-lock.json
COPY package*.json .
COPY yarn.lock .

# Install npm production packages 
RUN yarn --production

COPY src src

ENV NODE_ENV production
ENV PORT 3000

EXPOSE 3000

USER node

CMD ["yarn", "start"]
