FROM node:15-alpine

# Change working directory
WORKDIR "/app"

# Install OS updates
RUN apk upgrade

# Copy package.json, package-lock.json, yarn.lock files
COPY package*.json .
COPY yarn.lock .

# Install npm packages
RUN yarn

COPY src src

ENV NODE_HEAPDUMP_OPTIONS nosignal
ENV PORT 3000

EXPOSE 3000

USER node

CMD ["yarn", "dev"]
